﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour
{
    public static LevelController Instance;

    public Transform[] playerSpawnPoints = new Transform[4];

    private void Awake()
    {
        if (!Instance)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

        for (int i = 0; i < playerSpawnPoints.Length; i++)
        {
            if (!playerSpawnPoints[i])
            {
                Debug.LogError("Missing Spawnpoint: "+ playerSpawnPoints[i] + " on level" + SceneManager.GetActiveScene().name + ".");
            }
        }
    }
}
