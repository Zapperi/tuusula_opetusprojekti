﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyableObject : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem destroyedParticles;
    [SerializeField]
    private AudioClip destroyedSound;
    [SerializeField]
    private AudioSource audioSource;
    public int playerNumber;

    private void Start()
    {
        ParticleSystem.MainModule newMain;
        newMain = destroyedParticles.main;
        newMain.startColor = GameController.Instance.playerColorList[playerNumber];
    }

    public void PopBox()
    {
        Debug.Log("deleting");
        destroyedParticles.Play();
        audioSource.clip = destroyedSound;
        audioSource.pitch = (Random.Range(audioSource.pitch - 0.2f, audioSource.pitch + 0.2f));
        audioSource.Play();
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        gameObject.GetComponent<Collider>().enabled = false;
        gameObject.GetComponent<Rigidbody>().isKinematic = true;
        GameController.Instance.RemovePlayerLife(playerNumber, gameObject);
        Destroy(gameObject, 3f);
    }
}
