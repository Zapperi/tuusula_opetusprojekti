﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static GameController Instance;
    public GameObject pausePanel;
    [Range(1,4)]
    public int playerCount = 1;
    [Range(1, 100)]
    public int lifeCount = 10;
    public List<Color> playerColorList;
    public bool gameIsReady;

    private bool m_isGamePaused;
    public int[] playerLifeCounts = new int[4];
    public TextMeshProUGUI[] playerLifeCounters = new TextMeshProUGUI[4];
    private LevelController currentLevel;
    private GameObject[] playerGameObjects = new GameObject[4];

    public GameObject mainMenuCanvas;
    public GameObject gameHUD;
    public TextMeshProUGUI playerCountText;
    public Slider playerCountSlider;
    public TextMeshProUGUI lifeCountText;
    public Slider lifeCountSlider;
    public TextMeshProUGUI fireRateCountText;
    public Slider fireRateCountSlider;
    public float gunFireRates;

    public bool gameInMainMenu = true;
    public GameObject[] playerLivesPanels;
    public List<AudioClip> battleMusics;
    public AudioClip mainMenuMusic;
    public AudioSource musicSource;
    public GameObject playerPrefab;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        //Cursor.lockState = CursorLockMode.Locked;

        //InitLevel();

        // Placeholder level loader
        currentLevel = LevelController.Instance;
        //InitPlayers();

        
    }

    private void Start()
    {
        //InitPlayers();
        SceneManager.LoadScene(1, LoadSceneMode.Additive);
    }

    private void Update()
    {
        if (gameInMainMenu)
        {
            playerCount = Mathf.RoundToInt(playerCountSlider.value);
            playerCountText.text = playerCount.ToString();
            lifeCount = Mathf.RoundToInt(lifeCountSlider.value);
            lifeCountText.text = lifeCount.ToString();
            gunFireRates = fireRateCountSlider.value;
            fireRateCountText.text = gunFireRates.ToString("0.##");
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                ToggleGamePause();
            }
        }
    }

    public void ToggleGamePause()
    {
        if (!pausePanel.activeInHierarchy)
        {
            PauseGame();
        }
        else if (pausePanel.activeInHierarchy)
        {
            ContinueGame();
        }
    }

    public void PauseGame()
    {        
        m_isGamePaused = true;
        pausePanel.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 0.00001f;
    }

    public void ContinueGame()
    {
        m_isGamePaused = false;
        pausePanel.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Time.timeScale = 1f;
    }

    public bool IsGamePaused()
    {
        return m_isGamePaused;
    }

    public void QuitGame()
    {
    #if UNITY_EDITOR        
        UnityEditor.EditorApplication.isPlaying = false;
    #else
            Application.Quit();
    #endif
    }

    public void InitLevel()
    {
        for(int i = 0; i < playerLifeCounts.Length; ++i)
        {
            playerLifeCounts[i] = lifeCount;
            playerLifeCounters[i].text = playerLifeCounts[i].ToString();
        }
    }

    public void InitPlayers()
    {
        for(int i = 0; i < playerCount; i++)
        {
            //GameObject temp = Instantiate(Resources.Load<GameObject>("Prefabs/Player"), LevelController.Instance.playerSpawnPoints[i].position, Quaternion.identity, transform);
            GameObject temp = Instantiate(playerPrefab, LevelController.Instance.playerSpawnPoints[i].position, Quaternion.identity, transform);
            temp.GetComponent<PlayerMovement>().playerNumber = i;
            temp.GetComponent<PlayerMovement>().playerFireRate = gunFireRates;
            temp.name = "Player" + (i + 1);
            temp.transform.parent = null;
            playerGameObjects[i] = temp;
            CameraControl.Instance.m_Targets.Add(playerGameObjects[i].GetComponent<Transform>());
        }
    }

    public void RemovePlayerLife(int playerNumber, GameObject lifeCube)
    {
        playerLifeCounts[playerNumber] -= 1;
        ObjectSpawner.Instance.playerSpawnPools[playerNumber].Remove(lifeCube);
        playerLifeCounters[playerNumber].text = playerLifeCounts[playerNumber].ToString();
        if (playerLifeCounts[playerNumber] == 0)
        {
            playerGameObjects[playerNumber].SetActive(false);
        }
    }

    public void StartGame()
    {
        gameInMainMenu = false;
        Cursor.lockState = CursorLockMode.Locked;
        InitLevel();
        InitPlayers();
        ObjectSpawner.Instance.InitObjectSpawner();
        gameHUD.SetActive(true);
        for(int i = 0; i < playerCount; i++)
        {
            playerLivesPanels[i].SetActive(true);
        }
        mainMenuCanvas.SetActive(false);
        musicSource.clip = battleMusics[Mathf.RoundToInt(Random.Range(0f, battleMusics.Count-1))];
        musicSource.Play();
    }
}
