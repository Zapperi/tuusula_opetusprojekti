﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float playerMovementSpeed;
    public float mouseSensitivity;
    public GunController playerGun;
    public float sprintMultiplier = 2;
    public GameObject playerHelmet;
    [Range(0,3)]
    public int playerNumber;

    public float dashPower = 5;
    public float dashCooldDown = 3.0f;
    public float dashLenght = 0.5f;
    public float dashMass;
    public float dashDrag;

    public float jumpPower;
    public LayerMask jumpMask;
    private float m_torsoRadius;

    public Rigidbody m_playerRB;
    public Camera m_mainCamera;
    public Vector3 m_inputs;
    public bool m_playerDashing;
    public bool m_playerGrounded;
    public string playerInputNumber;

    public float playerFireRate;

    
    void Awake()
    {
        //if(m_playerRB == null)
        //{
        //    m_playerRB = GetComponent<Rigidbody>();            
        //}
        //else
        //{
        //    Debug.Log("Player RigidBody already set at: " + name + "!");            
        //}
        //m_mainCamera = Camera.main;

        //m_torsoRadius = GetComponentInChildren<CapsuleCollider>().height / 2;
        //Debug.Log("Player number: " + playerNumber);
        //playerInputNumber = "Player"+(playerNumber+1).ToString();
        //Debug.Log("Player Input Number: " + playerInputNumber);
    }

    private void Start()
    {
        StartCoroutine(LateAwake(0.2f));
        playerHelmet.GetComponent<MeshRenderer>().material.SetColor("_BaseColor", GameController.Instance.playerColorList[playerNumber]);
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameController.Instance.IsGamePaused() && GameController.Instance.gameIsReady)
        {
            // Check if the player has pressed the fire button and if enough time has elapsed since they last fired
            if (Input.GetAxis(playerInputNumber + "Fire") > 0f)
            {
                playerGun.FireGun();
            }

            m_inputs = transform.TransformDirection(Input.GetAxis(playerInputNumber + "Horizontal"), 0f, Input.GetAxis(playerInputNumber + "Vertical"));
            if (Input.GetAxis(playerInputNumber + "Sprint") > 0f && m_playerGrounded)
            {
                m_inputs *= sprintMultiplier;
            }
            if (Input.GetButtonDown(playerInputNumber+"Jump") && m_playerGrounded)
            {            
                m_PlayerJump();
            }
            if (Input.GetButtonDown(playerInputNumber + "Dash") && !m_playerDashing)
            {
                StartCoroutine(m_PlayerDash(dashPower, dashCooldDown, dashLenght));
            }
        }
    }

    private void FixedUpdate()
    {
        if (GameController.Instance.gameIsReady)
        {
            m_playerGrounded = IsPlayerGrounded();
            m_MovePlayer();
        }
    }

    public bool IsPlayerGrounded()
    {
        return Physics.CheckSphere(transform.position, m_torsoRadius + 0.01f, jumpMask, QueryTriggerInteraction.Ignore);
    }

    private void m_MovePlayer()
    {
        m_playerRB.MovePosition(m_playerRB.position + m_inputs * playerMovementSpeed * Time.fixedDeltaTime);
        m_playerRB.MoveRotation(Quaternion.Euler(transform.eulerAngles + new Vector3(0f, Input.GetAxis(playerInputNumber + "Turn"), 0f) * mouseSensitivity * Time.fixedDeltaTime));
    }

    public void MovePlayer(Vector3 addedMovement, Vector3 addedRotation)
    {
        m_playerRB.MovePosition(m_playerRB.position + addedMovement * playerMovementSpeed * Time.fixedDeltaTime);
        m_playerRB.MoveRotation(Quaternion.Euler(transform.eulerAngles + addedRotation));
    }

    private void m_PlayerJump()
    {
        m_playerRB.AddForce(Vector3.up * jumpPower, ForceMode.Impulse);
    }

    private IEnumerator m_PlayerDash(float speed, float dashCoolDown, float dashLenght)
    {
        m_playerDashing = true;
        float oldMass = m_playerRB.mass;
        float oldDrag = m_playerRB.drag;
        m_playerRB.drag = dashDrag;
        m_playerRB.mass = dashMass;

        Vector3 dashVelocity = Vector3.Scale(transform.forward, speed * new Vector3((Mathf.Log(1f / (Time.deltaTime * m_playerRB.drag + 1)) / -Time.deltaTime), 0, (Mathf.Log(1f / (Time.deltaTime * m_playerRB.drag + 1)) / -Time.deltaTime)));
        m_playerRB.AddForce(dashVelocity, ForceMode.VelocityChange);

        float endTime = Time.time + dashCoolDown;
        float dashTime = Time.time + dashLenght;
        while (endTime >= Time.time)
        {
            if (Time.time >= dashTime && m_playerRB.drag > 1)
            {
                m_playerRB.drag = oldDrag;
                m_playerRB.mass = oldMass;
            }
            yield return null;
        }
        m_playerDashing = false;
    }

    IEnumerator LateAwake(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);

        if (m_playerRB == null)
        {
            m_playerRB = GetComponent<Rigidbody>();
        }
        else
        {
            Debug.Log("Player RigidBody already set at: " + name + "!");
        }
        m_mainCamera = Camera.main;

        m_torsoRadius = GetComponentInChildren<CapsuleCollider>().height / 2;
        playerInputNumber = "Player" + (playerNumber + 1).ToString();
    }
}
