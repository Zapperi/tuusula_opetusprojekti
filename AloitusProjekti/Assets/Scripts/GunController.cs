﻿// NO YET ADDED TO GUIDE
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

// RayCasting based on tutorial code from Unity
// https://learn.unity.com/tutorial/let-s-try-shooting-with-raycasts

public class GunController : MonoBehaviour
{
    public float fireRate = 0.25f;                                        // Number in seconds which controls how often the player can fire
    public float weaponRange = 50f;                                        // Distance in Unity units over which the player can fire
    public float hitForce = 10f;                                        // Amount of force which will be added to objects with a rigidbody shot by the player
    public Transform bulletSpawnPoint;
    public ParticleSystem shotHitEffect;
    public ParticleSystem gunFireEffect;
    public AudioClip gunFireSound;
    public List<AudioSource> audioSourceList = new List<AudioSource>();
    public GameObject gunFireLight;

    //private Camera fpsCam;                                                // Holds a reference to the first person camera
    public float gunFireOriginalPitch;
    public WaitForSeconds shotDuration = new WaitForSeconds(0.1f);    // WaitForSeconds object used by our ShotEffect coroutine, determines time laser line will remain visible
    public AudioSource gunAudio;                                        // Reference to the audio source which will play our shooting sound effect
    public LineRenderer laserLine;                                        // Reference to the LineRenderer component which will display our laserline
    public float nextFire;                                                // Float to store the time the player will be allowed to fire again, after firing
    public int m_playerNumber;
    public ParticleSystem.MainModule newMain;

    void Start()
    {
        StartCoroutine(LateStart(0.5f));
        //Color playerColor = GameController.Instance.playerColorList[m_playerNumber];
        //// Get and store a reference to our LineRenderer component
        //laserLine = GetComponent<LineRenderer>();

        //// Get and store a reference to our AudioSource component
        //gunAudio = GetComponent<AudioSource>();

        //m_playerNumber = transform.root.GetComponent<PlayerMovement>().playerNumber;
        //foreach(Light light in gunFireLight.GetComponentsInChildren<Light>())
        //{
        //    light.color = playerColor;
        //}
        //laserLine.endColor = playerColor;
        //laserLine.startColor = playerColor;

        //newMain = shotHitEffect.main;
        //newMain.startColor = playerColor;
        //newMain = gunFireEffect.main;
        //newMain.startColor = playerColor;

        //gunFireOriginalPitch = audioSourceList[0].pitch;
    }


    //void Update()
    //{
    //    // Check if the player has pressed the fire button and if enough time has elapsed since they last fired
    //    if (Input.GetButtonDown("Player1Fire") && (Time.time > nextFire) && !GameController.Instance.IsGamePaused())
    //    {
    //        FireGun();
    //    }
    //}


    public void FireGun()
    {
         if (Time.time > nextFire)
        {
            // Update the time when our player can fire next
            nextFire = Time.time + fireRate;

            // Start our ShotEffect coroutine to turn our laser line on and off
            StartCoroutine(ShotEffect());

            for (int i = 0; i < audioSourceList.Count; ++i)
            {
                if (!audioSourceList[i].isPlaying)
                {
                    AudioSource temp = audioSourceList[i];
                    temp.pitch = UnityEngine.Random.Range(gunFireOriginalPitch - 0.1f, gunFireOriginalPitch + 0.1f);
                    temp.clip = gunFireSound;
                    temp.Play();
                    break;
                }
                else if (i == audioSourceList.Count - 1)
                {
                    audioSourceList.Add(gameObject.AddComponent<AudioSource>(audioSourceList[0]));
                    AudioSource temp = audioSourceList.Last();
                    temp.pitch = UnityEngine.Random.Range(gunFireOriginalPitch - 0.1f, gunFireOriginalPitch + 0.1f);
                    temp.clip = gunFireSound;
                    temp.time = Mathf.Min(3f, temp.clip.length - 0.01f);
                    temp.Play();
                    break;
                }
            }

            // Create a vector at the center of our camera's viewport
            Vector3 bulletSpawnForward = bulletSpawnPoint.transform.TransformDirection(Vector3.forward);

            // Declare a raycast hit to store information about what our raycast has hit
            RaycastHit hit;

            // Set the start position for our visual effect for our laser to the position of gunEnd
            laserLine.SetPosition(0, bulletSpawnPoint.position);

            // Check if our raycast has hit anything
            if (Physics.Raycast(bulletSpawnPoint.position, bulletSpawnForward, out hit, weaponRange))
            {
                // Set the end position for our laser line 
                laserLine.SetPosition(1, hit.point);

                #region Hit Particle effects

                ParticleSystem shotHit = Instantiate<ParticleSystem>(shotHitEffect, hit.point, Quaternion.Euler(-bulletSpawnForward), hit.transform);
                shotHit.transform.parent = null;
                Destroy(shotHit.gameObject, shotHit.main.duration);

                //Debug.Log("Hit object color: " + hit.collider.GetComponent<Renderer>().material.color);
                //Debug.Log("Hit object COLLIDER name: " + hit.rigidbody.gameObject.GetComponent<Renderer>().sharedMaterial.color);
                ParticleSystem gunFired = Instantiate<ParticleSystem>(gunFireEffect, hit.transform);
                newMain = gunFired.main;
                newMain.startSize = newMain.startSize.constant * 2;
                //newMain.startColor = GameController.Instance.playerColorList[m_playerNumber];
                newMain.startColor = hit.collider.GetComponent<Renderer>().material.GetColor("_BaseColor");
                //gunFired.main.startColor = newMain;
                //gunFired.startSize =+ gunFired.startSize * 2;
                gunFired.transform.parent = null;
                gunFired.emission.SetBurst(0, new ParticleSystem.Burst(0.0f, 2, 5));
                gunFired.Play();
                Destroy(gunFired.gameObject, gunFired.main.duration / 2);

                #endregion

                var destroyableObject = hit.collider.GetComponent<DestroyableObject>();
                if (destroyableObject)
                {
                    destroyableObject.PopBox();
                }

                // Check if the object we hit has a rigidbody attached
                if (hit.rigidbody != null)
                {
                    Debug.Log("RayCast hit: " + hit.rigidbody.name);
                    // Add force to the rigidbody we hit, in the direction from which it was hit
                    hit.rigidbody.AddForce(-hit.normal * hitForce, ForceMode.VelocityChange);
                }
            }
            else
            {
                // If we did not hit anything, set the end of the line to a position directly in front of the camera at the distance of weaponRange
                laserLine.SetPosition(1, bulletSpawnPoint.position + (bulletSpawnForward * weaponRange));
            }
        }          
    }

    private IEnumerator ShotEffect()
    {
        ParticleSystem gunFiredEffect = Instantiate<ParticleSystem>(gunFireEffect, bulletSpawnPoint);
        gunFiredEffect.transform.parent = null;
        gunFiredEffect.Play();
        Destroy(gunFiredEffect.gameObject, gunFiredEffect.main.duration/2);
        // Turn on our line renderer
        laserLine.enabled = true;
        gunFireLight.gameObject.SetActive(true);

        //Wait for .07 seconds
        yield return shotDuration;

        // Deactivate our line renderer after waiting
        gunFireLight.gameObject.SetActive(false);
        laserLine.enabled = false;
    }

    IEnumerator LateStart(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        m_playerNumber = transform.root.GetComponent<PlayerMovement>().playerNumber;
        fireRate = transform.root.GetComponent<PlayerMovement>().playerFireRate;
        Color playerColor = GameController.Instance.playerColorList[m_playerNumber];

        // Get and store a reference to our LineRenderer component
        laserLine = GetComponent<LineRenderer>();

        // Get and store a reference to our AudioSource component
        gunAudio = GetComponent<AudioSource>();

        foreach (Light light in gunFireLight.GetComponentsInChildren<Light>())
        {
            light.color = playerColor;
        }
        laserLine.endColor = playerColor;
        laserLine.startColor = playerColor;

        newMain = shotHitEffect.main;
        newMain.startColor = playerColor;
        newMain = gunFireEffect.main;
        newMain.startColor = playerColor;

        gunFireOriginalPitch = audioSourceList[0].pitch;

        GameController.Instance.gameIsReady = true;
    }
}
    