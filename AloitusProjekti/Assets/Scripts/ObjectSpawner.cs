﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    public static ObjectSpawner Instance;
    public GameObject spawnableObject1;
    //public int object1Ammount;
    //public GameObject spawnableObject2;
    //public int object2Ammount;
    public float bufferSize;

    public List<GameObject>[] playerSpawnPools = new List<GameObject>[4];
    public List<GameObject> player1SpawnPool;
    public List<GameObject> player2SpawnPool;
    public List<GameObject> player3SpawnPool;
    public List<GameObject> player4SpawnPool;

    private Vector3 m_spawnPosition;
    private float m_spawnAreaMinWidth;
    private float m_spawnAreaMaxWidth;
    private float m_spawnAreaMinLenght;
    private float m_spawnAreaMaxLenght;

    public MeshRenderer floorMR;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        //m_SetBoundaries();
        //m_SpawnObjects();
        //InitPlayerSpawnPools();
    }

    //private void m_SpawnObjects()
    //{
    //    for (int i = 0; object1Ammount >= i; ++i)
    //    {
    //        m_spawnPosition = new Vector3(Random.Range(m_spawnAreaMinWidth, m_spawnAreaMaxWidth), transform.position.y, Random.Range(m_spawnAreaMinLenght, m_spawnAreaMaxLenght));
    //        Instantiate(spawnableObject1, m_spawnPosition, Quaternion.Euler(new Vector3(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360))), this.transform);
    //    }
    //    for (int i = 0; object2Ammount >= i; ++i)
    //    {
    //        m_spawnPosition = new Vector3(
    //            Random.Range(m_spawnAreaMinWidth, m_spawnAreaMaxWidth), transform.position.y, Random.Range(m_spawnAreaMinLenght, m_spawnAreaMaxLenght));
    //        Instantiate(spawnableObject2, m_spawnPosition, Quaternion.Euler(
    //            new Vector3(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360))), this.transform);
    //    }
    //}

    public void InitObjectSpawner()
    {
        m_SetBoundaries();
        InitPlayerSpawnPools();
    }

    private void m_SetBoundaries()
    {
        floorMR = GameObject.Find("Floor").GetComponent<MeshRenderer>();
        m_spawnAreaMinLenght = floorMR.bounds.min.x + bufferSize;
        m_spawnAreaMaxLenght = floorMR.bounds.max.x - bufferSize;
        m_spawnAreaMinWidth = floorMR.bounds.min.z + bufferSize;
        m_spawnAreaMaxWidth = floorMR.bounds.max.z - bufferSize;
    }

    private void InitPlayerSpawnPools()
    {
        playerSpawnPools[0] = player1SpawnPool;
        playerSpawnPools[1] = player2SpawnPool;
        playerSpawnPools[2] = player3SpawnPool;
        playerSpawnPools[3] = player4SpawnPool;

        for (int i = 0; i < GameController.Instance.playerCount; i++)
        {
            for(int k = 0; k < GameController.Instance.lifeCount; k++)
            {
                m_spawnPosition = new Vector3(Random.Range(m_spawnAreaMinWidth, m_spawnAreaMaxWidth), transform.position.y, Random.Range(m_spawnAreaMinLenght, m_spawnAreaMaxLenght));
                GameObject newSpawn = Instantiate(spawnableObject1, m_spawnPosition, Quaternion.Euler(new Vector3(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360))), GameController.Instance.transform);

                //GameObject newSpawn = Instantiate<GameObject>(spawnableObject1, GameController.Instance.transform);
                newSpawn.GetComponent<MeshRenderer>().material.SetColor("_BaseColor", GameController.Instance.playerColorList[i]);
                newSpawn.GetComponent<DestroyableObject>().playerNumber = i;
                playerSpawnPools[i].Add(newSpawn);
            }
        }
    }
}
